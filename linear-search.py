import sys

def search(n,arr,x):
	for i in range(0,n):
		if arr[i] == x:
			return i
	return -1


if __name__ == '__main__':
	while True:
		print("Enter the Array values ( Press Ctr + C to stop)")
		user_inputs=[]
		while True:
			try:
				user_inputs.append(input())
			except EOFError:
				user_inputs_str= "\n".join(user_inputs)
				print(user_inputs_str)
				break
		N=len(user_inputs)
		x=input("Enter the value to search")
		if search(N,user_inputs,x) != -1:

			print(str(x)+" is in list and at index "+str(search(N,user_inputs,x)))

		else:
			print(str(x)+" is not in the list")

		
		