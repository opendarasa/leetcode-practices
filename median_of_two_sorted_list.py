def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        combined=sorted(nums1 + nums2)
        while len(combined) > 2 :
            combined.pop(0)
            combined.pop()
        if len(combined)==1:
            return float(combined[0])
        else:
            return (combined[0]+combined[1])/2