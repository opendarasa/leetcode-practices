def binary_search(x,arr,low,high):
	if low > high:
		return -1
	else:
		mid = low + (high-low)//2 # floor division to get the middle of the array
		if arr[mid] == x:
			return mid
		elif x > arr[mid] :
			return binary_search(x,arr,mid + 1 ,high)
		else :
			return binary_search(x,arr,low,mid-1)

if __name__ == '__main__':
	
	while True:
		user_inputs=[]
		print("Enter the Variable of the Array (Press Ctrl + D to stop entering)")
		while True:
			
			try:
				user_inputs.append(input())
			except EOFError:
				print(user_inputs)
				break
		x_val=input("Enter the value you would like to search: ")
		
		#user_inputs.sort()
		res=binary_search(x_val,user_inputs,0,len(user_inputs)-1)

		if res != -1:
			print(str(x_val)+" exist in the list at index "+str(res))
		else:
			print(str(x_val)+" does not exist in the list")
			