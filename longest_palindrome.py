def longestPalindrome(self, s: str) -> str:
    res=""
    for i in range(len(s)):
        for j in range(i,i+2):
            k=i
            l=j
            while k>=0 and l<len(s) and s[k]==s[l]:
                k-=1
                l+=1
            palindrome_len=l-k-1
            if palindrome_len>len(res):
                res=s[k+1:l]
    return res