# cook your dish here
def tax(x):
    if x > 10 :
        return x - 10
    else: return x


if __name__== "__main__":
    t=input()
    x_list=[]
    if t.strip().isdigit() and int(t)>= 1 and int(t) <=100 : 
        for i in range(0,t+1):
            x=input()
            if x.strip().isdigit() and x >= 1 and x <= 1000:
                x_list.append(int(x))
            else:
                continue
        for i in x_list:
            print(tax(i))
            
    else:
        print("invalid input")
        