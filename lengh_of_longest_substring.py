def lengthOfLongestSubstring(self, s: str) -> int:
    hold=[]
    hold=list(s)
    max_len=0
    insert=""
    if len(s)<=1:
        return len(s)
    for i in range(0,len(hold)):
        j=i+1
        insert+=hold[i]
        while j<=len(hold)-1 and hold[j] not in insert:
            insert+=hold[j]
            j+=1
        max_len=max(max_len,len(insert))
        insert=""

    return max_len